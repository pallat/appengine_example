module gitlab.com/pallat/example

go 1.13

require (
	cloud.google.com/go v0.55.0
	cloud.google.com/go/datastore v1.1.0
	cloud.google.com/go/pubsub v1.3.1
	cloud.google.com/go/storage v1.6.0
	contrib.go.opencensus.io/exporter/stackdriver v0.13.0
	github.com/carlescere/scheduler v0.0.0-20170109141437-ee74d2f83d82
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.7.4
	github.com/labstack/echo/v4 v4.1.16
	github.com/spf13/viper v1.6.2
	go.opencensus.io v0.22.3
)
